package file_generator

import (
	"bufio"
	"fmt"
	"github.com/cheggaaa/pb/v3"
	"math/rand"
	"os"
	"sync"
	"time"
	"unsafe"
)

var charBegin = 32
var charEnd = 128
var symbolsLen int
var symbols []byte

var randomSrc = rand.NewSource(time.Now().UnixNano())
var randomSrcLock sync.Mutex
func Initialize(lettersOnly bool) {
	if lettersOnly {
		charBegin = 65
		charEnd = 91
	}
	symbolsLen = charEnd - charBegin + 1 /*EOL*/
	symbols = make([]byte, symbolsLen)
	for i := charBegin; i < charEnd; i++ {
		symbols[i - charBegin] = byte(i)
	}
	symbols[symbolsLen - 1] = 0
}

func GenerateString(maxLen int) string {
	res := make([]byte, maxLen)
	for index := 0; index < maxLen; index++ {
		randomSrcLock.Lock()
		res[index] = symbols[randomSrc.Int63() % int64(symbolsLen)]
		randomSrcLock.Unlock()
		if res[index] == 0 {
			res = res[:index]
			break
		}
	}
	return *(*string)(unsafe.Pointer(&res))
}

const workerNum = 8
const bufSize = 100000000
const barThreshold = 10192

func GenerateTextToFile(N, maxStrLen int, file *os.File) {
	done := make(chan bool, workerNum)
	started := make(chan bool, workerNum)
	var workerWG sync.WaitGroup
	workerWG.Add(workerNum)
	for i := 0; i < workerNum; i++ {
		go worker(started, done, file, maxStrLen, &workerWG)
	}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		bar := pb.StartNew(N)
		for i := 0; i < N; i++ {
			<- done
			if (i + 1) % barThreshold == 0 {
				bar.Add(barThreshold)
			}
		}
		bar.Add(N % barThreshold)
		bar.Finish()
		wg.Done()
	}()

	for i := 0; i < N; i++ {
		started <- true
	}

	wg.Wait()
	close(done)
	close(started)
	workerWG.Wait()
}

func worker(jobs <-chan bool, done chan<- bool, file *os.File, maxStrLen int, workerWg *sync.WaitGroup) {
	writer := bufio.NewWriterSize(file, bufSize)
	for _ = range jobs {
		str := GenerateString(maxStrLen)
		fmt.Fprintln(writer, str)
		if writer.Available() <= maxStrLen {
			writer.Flush()
		}
		done <- true
	}
	writer.Flush()
	workerWg.Done()
}