package job_pipeline

import "sync"

type PipelineError struct {
	s string
}

type Job func(in, out chan interface{})

func (pe *PipelineError) Error() string {
	return pe.s
}

func ExecutePipeline(maxInputDataLen int, jobs ...Job) {
	var from chan interface{}
	var wait sync.WaitGroup
	for _, singleJob := range jobs {
		to := make(chan interface{}, maxInputDataLen)
		wait.Add(1)
		go func(chan1, chan2 chan interface{}, oneJob Job) {
			oneJob(chan1, chan2)
			close(chan2)
			wait.Done()
		}(from, to, singleJob)
		from = to
	}
	wait.Wait()
}
