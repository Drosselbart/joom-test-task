package main

import (
	"flag"
	"fmt"
	file_generator "joom-test-task/file-generator"
	"os"
)

var filePath = flag.String("o", "out.txt", "filename for result output")
var N = flag.Int("n", -1, "number of lines in output")
var maxStrLen = flag.Int("maxStrLen", -1, "maximal single line len for output")
var lettersOnly = flag.Bool("LO", false, "set this flag if you wan't see only letters in output")

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
			fmt.Println("Congratulations! You've found a bug! Submit it as an issue")
		}
	}()

	flag.Parse()
	if *N < 0 || *maxStrLen < 0 {
		fmt.Println("provide both N and maxStrLen args")
		flag.Usage()
		return
	}
	file, err := os.Create(*filePath)
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			fmt.Println(err.Error())
			os.Exit(-1)
		}
	}(file)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	file_generator.Initialize(*lettersOnly)
	file_generator.GenerateTextToFile(*N, *maxStrLen, file)
}
