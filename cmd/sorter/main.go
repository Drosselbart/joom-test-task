package main

import (
	"flag"
	"fmt"
	"joom-test-task/sorter"
	"os"
)

var inPath = flag.String("i", "in.txt", "filename for input")

var outPath = flag.String("o", "out.txt", "filename for result output")

var portionSize = flag.Int("n", 1000000, "number of lines that may be read into memory")

var tmpPath = flag.String("tmpPath", os.TempDir(), "path to create tmp directory where will be placed files with sorted sub-arrays")


func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
			fmt.Println("Congratulations! You've found a bug! Submit it as an issue")
		}
	}()
	flag.Parse()
	file, err := os.Open(*inPath)
	defer file.Close()
	if err != nil {
		fmt.Println(err.Error())
		flag.Usage()
		return
	}
	err = os.MkdirAll(*tmpPath, 0777)
	if err != nil {
		fmt.Println(err.Error())
		flag.Usage()
		return
	}
	defer os.RemoveAll(*tmpPath)
	sorter.SortStringsFile(file, *portionSize, *tmpPath, *outPath)
}
