package sorter

import (
	"bufio"
	"flag"
	"fmt"
	job_pipeline "joom-test-task/job-pipeline"
	"os"
	"sort"
)

const partiallySortedPrefix = "partially-sorted-"

func newTmpFile(tmpPath string) *os.File {
	tmpFile, err := os.CreateTemp(tmpPath, partiallySortedPrefix)
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("please provide correct additional tmpPath argument")
		flag.Usage()
		os.Exit(0)
	}
	return tmpFile
}

func minIndex(strs []string) int {
	minStr := strs[0]
	minI := 0
	for i, str := range strs {
		if str <= minStr {
			minI = i
		}
	}
	return minI
}

//TODO: with heap
func mergeFiles(files []*os.File, tmpDirName string) *os.File {
	scanners := make([]*bufio.Scanner, len(files))
	strs := make([]string, len(files))
	outFile := newTmpFile(tmpDirName)
	for i, file := range files {
		scanners[i] = bufio.NewScanner(file)
		scanners[i].Scan()
		strs[i] = scanners[i].Text()
	}
	for len(scanners) > 0 {
		i := minIndex(strs)
		fmt.Fprintln(outFile, strs[i])
		if scanners[i].Scan() {
			strs[i] = scanners[i].Text()
		} else {
			if i == 0 {
				scanners = scanners[1:]
				strs = strs[1:]
			} else if i == len(scanners) - 1 {
				scanners = scanners[:len(scanners) - 1]
				strs = strs[:len(strs) - 1]
			} else {
				scanners = append(scanners[:i], scanners[i+1:]...)
				strs = append(strs[:i], strs[i+1:]...)
			}
		}
	}
	outFile.Seek(0,0)
	return outFile
}

func SortStringsFile(file *os.File, portion int, tmpDirPath, outName string) {
	scanner := bufio.NewScanner(file)
	sortLargeFileJobs := []job_pipeline.Job{
		job_pipeline.Job(func(in, out chan interface{}) {
			i := 0
			lines := make([]string, 0, portion)
			for scanner.Scan() {
				lines = append(lines, scanner.Text())
				i++
				if i == portion {
					out <- lines
					lines = make([]string, 0, portion)
					i = 0
				}
			}
			out <- lines
		}),
		job_pipeline.Job(func(in, out chan interface{}) {
			for linesI := range in {
				lines := linesI.([]string)
				sort.Strings(lines)
				out <- lines
			}
		}),
		job_pipeline.Job(func(in, out chan interface{}) {
			for linesI := range in {
				lines := linesI.([]string)
				if len(lines) > 0 {
					f := newTmpFile(tmpDirPath)
					for _, line := range lines {
						fmt.Fprintln(f, line)
					}
					f.Seek(0,0)
					out <- f
				}
			}
		}),
		job_pipeline.Job(func(in, out chan interface{}) {
			var files []*os.File
			var previouslyMergedFiles []*os.File
			for fileI := range in {
				file := fileI.(*os.File)
				files = append(files, file)
				if len(files) > portion {
					previouslyMergedFiles = append(previouslyMergedFiles, mergeFiles(files, tmpDirPath))
					if len(previouslyMergedFiles) > portion {
						previouslyMergedFiles = []*os.File{mergeFiles(previouslyMergedFiles, tmpDirPath)}
					}
					files = nil
				}
			}
			previouslyMergedFiles = append(previouslyMergedFiles, mergeFiles(files, tmpDirPath))
			outFile := mergeFiles(previouslyMergedFiles, tmpDirPath)
			os.Rename(outFile.Name(), outName)
		}),
	}
	job_pipeline.ExecutePipeline(1, sortLargeFileJobs...)
}